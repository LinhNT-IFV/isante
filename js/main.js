$(document).ready(function() {
	function smenuToggle()
	{
		var _nav  = $('.menu_box_title');
		var _item = $('.menu_box_toggle');
		function hide() {
			_item.hide();
		}
		function activeTab(obj)
		{
		    $(obj).toggleClass('active');
		    var id = $(obj).attr('href');
		    $(id).toggleClass('show');
		}
		activeTab($('.menu_box_trial .menu_box_title'));
		_nav.click(function(event){
			event.preventDefault();
		    activeTab(this);
		    return false;
		});
	}
	menuToggle();
	function wows() {
		wow = new WOW(
		{
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
	}
	wows();

	function mega_reponsive() {
		var btn  = $('nav .nav_btn');
		var mega = $('.nav_mega');
		btn.click(function() {
			btn.toggleClass('btn_show', 'last_show');
			mega.toggleClass('show');
			$('body').toggleClass('overflow');
		});
	}
	// mega_reponsive();
});